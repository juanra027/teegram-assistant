// This sample demonstrates handling intents from an Alexa skill using the Alexa Skills Kit SDK (v2).
// Please visit https://alexa.design/cookbook for additional examples on implementing slots, dialog management,
// session persistence, api calls, and more.
const Alexa = require('ask-sdk-core');
const request = require('request');
var user = null;

const LaunchRequestHandler = {
    canHandle(handlerInput) {
        return Alexa.getRequestType(handlerInput.requestEnvelope) === 'LaunchRequest';
    },
    handle(handlerInput) {
        const speakOutput = 'Welcome to Alexa Telegram Assistant. You can say use the code or log me in with. And after tell me your code';
        return handlerInput.responseBuilder
            .speak(speakOutput)
            .reprompt(speakOutput)
            .getResponse();
    }
};
const LogInIntentHandler = {
    canHandle(handlerInput) {
        const request = handlerInput.requestEnvelope.request;
        return Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest'
        && Alexa.getIntentName(handlerInput.requestEnvelope) === 'LogInIntent';
    },
    async handle(handlerInput) {
        
        var speakOutput='not in';
        const filledSlots = handlerInput.requestEnvelope.request.intent.slots;
        var code = filledSlots.code.value
        
        let data ={
                code:code
        }

        try{
            user = await doRequest('https://telegram-assistant-server.herokuapp.com/telegram/login', data)
        
            speakOutput = 'Welcome '+ user.name + '. You can send a message to one group. For Example. Send a message to Cars lovers that says Hello Group'
        } catch (err){
            speakOutput = 'Failed to LogIn'
        }
        /*if(res){
            speakOutput = 'First try'
        }
        else{
            
        }*/
        
        //const slotValues = Alexa.getSlotValues(Alexa.filledSlots);
        
        //const slotValues = Alexa.getSlotValues(filledSlots);
        
        //if (slotValues.code.resolved!== "?" && slotValues.code.resolved!== "?"){
        //    code = parseFloat(slotValues.code.resolved);
        //}
        
        return handlerInput.responseBuilder
            .speak(speakOutput)
            .reprompt(speakOutput)
            .getResponse();
    }
};

const SendMessageIntentHandler = {
    canHandle(handlerInput) {
        const request = handlerInput.requestEnvelope.request;
        return Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest'
        && Alexa.getIntentName(handlerInput.requestEnvelope) === 'SendMessageIntent';
    },
    async handle(handlerInput) {
        
        var speakOutput='not in';
        const filledSlots = handlerInput.requestEnvelope.request.intent.slots;
        var group = null;
        group = filledSlots.group.value
        var message = filledSlots.message.value
        let groupId = null;
        
        if(user === null){
            speakOutput = 'You first need to Log In'
        }
        
        else{
            user.groups.forEach(element => {
                if (element.name.toLowerCase() == group.toLowerCase()){
                    speakOutput = "lo encuentra"
                    groupId= element._id;
                    
                }
            });
            
            let data = {
                message: user.name+': '+message,
                chat: groupId,
                code:user.code
            }
            if(data.chat !== null && data.chat !== undefined && data.message !== undefined && data.message!== null){
                try{
                    await doRequest('https://telegram-assistant-server.herokuapp.com/telegram/sendMessage', data)
                    speakOutput = 'send it'
                } catch (err){
                    speakOutput = 'Failed to send it'
                }
            }
            else{
                speakOutput = 'Sorry I does not understand, can you please repeat'
                return handlerInput.responseBuilder
                .speak(speakOutput)
                .reprompt(speakOutput)
                .getResponse();
            }
            
            
        }
        
        return handlerInput.responseBuilder
            .speak(speakOutput)
            .reprompt(speakOutput)
            .getResponse();
    }
};


const SendImageIntentHandler = {
    canHandle(handlerInput) {
        const request = handlerInput.requestEnvelope.request;
        return Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest'
        && Alexa.getIntentName(handlerInput.requestEnvelope) === 'SendImageIntent';
    },
    async handle(handlerInput) {
        let speakOutput = 'image'
        
        return handlerInput.responseBuilder
            .speak(speakOutput)
            .reprompt(speakOutput)
            .getResponse();
    }
};

const HelpIntentHandler = {
    canHandle(handlerInput) {
        return Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest'
            && Alexa.getIntentName(handlerInput.requestEnvelope) === 'AMAZON.HelpIntent';
    },
    handle(handlerInput) {
        const speakOutput = 'You can say Use the code and after tell me the code sended to your phone';

        return handlerInput.responseBuilder
            .speak(speakOutput)
            .reprompt(speakOutput)
            .getResponse();
    }
};
const CancelAndStopIntentHandler = {
    canHandle(handlerInput) {
        return Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest'
            && (Alexa.getIntentName(handlerInput.requestEnvelope) === 'AMAZON.CancelIntent'
                || Alexa.getIntentName(handlerInput.requestEnvelope) === 'AMAZON.StopIntent');
    },
    handle(handlerInput) {
        const speakOutput = 'Goodbye!';
        return handlerInput.responseBuilder
            .speak(speakOutput)
            .getResponse();
    }
};
const SessionEndedRequestHandler = {
    canHandle(handlerInput) {
        return Alexa.getRequestType(handlerInput.requestEnvelope) === 'SessionEndedRequest';
    },
    handle(handlerInput) {
        // Any cleanup logic goes here.
        return handlerInput.responseBuilder.getResponse();
    }
};

// The intent reflector is used for interaction model testing and debugging.
// It will simply repeat the intent the user said. You can create custom handlers
// for your intents by defining them above, then also adding them to the request
// handler chain below.
const IntentReflectorHandler = {
    canHandle(handlerInput) {
        return Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest';
    },
    handle(handlerInput) {
        const intentName = Alexa.getIntentName(handlerInput.requestEnvelope);
        const speakOutput = `You just triggered ${intentName}`;

        return handlerInput.responseBuilder
            .speak(speakOutput)
            //.reprompt('add a reprompt if you want to keep the session open for the user to respond')
            .getResponse();
    }
};

// Generic error handling to capture any syntax or routing errors. If you receive an error
// stating the request handler chain is not found, you have not implemented a handler for
// the intent being invoked or included it in the skill builder below.
const ErrorHandler = {
    canHandle() {
        return true;
    },
    handle(handlerInput, error) {
        console.log(`~~~~ Error handled: ${error.stack}`);
        const speakOutput = `Sorry, I had trouble doing what you asked. Please try again.`;

        return handlerInput.responseBuilder
            .speak(speakOutput)
            .reprompt(speakOutput)
            .getResponse();
    }
};

function doRequest(url,data) {
    //if(data!==null){
        return new Promise(function (resolve, reject) {
        request.post(url,{
            json: data
            
        }, (error, res, body) => {
          if (!error && res.statusCode === 200) {
            resolve(body.data);
          } else {
            reject(error);
          }
        });
      });
}

// The SkillBuilder acts as the entry point for your skill, routing all request and response
// payloads to the handlers above. Make sure any new handlers or interceptors you've
// defined are included below. The order matters - they're processed top to bottom.
exports.handler = Alexa.SkillBuilders.custom()
    .addRequestHandlers(
        LaunchRequestHandler,
        //HelloWorldIntentHandler,
        HelpIntentHandler,
        CancelAndStopIntentHandler,
        SessionEndedRequestHandler,
        SendMessageIntentHandler,
        SendImageIntentHandler,
        
        LogInIntentHandler,
        //IntentReflectorHandler, // make sure IntentReflectorHandler is last so it doesn't override your custom intent handlers
        
    )
    .addErrorHandlers(
        ErrorHandler,
    )
    .lambda();
